package com.sendev.cowjoinmessages;

import com.sendev.cowjoinmessages.utils.Permissions;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class CowReloadCommand implements CommandExecutor
{

    /**
     * The main file instance, allows us to talk
     * to the server more easily, reload the
     * config and check messages.. etc..
     *
     * @var CowJoinmessages plugin
     */
    private final CowJoinMessages plugin;

    /**
     * Creates a new instance of the Cow Reload Command
     * class, and sets our plugin object.
     *
     * @param CowJoinmessages plugin
     */
    public CowReloadCommand(CowJoinMessages plugin)
    {
        this.plugin = plugin;
    }

    /**
     * The Bukkit/Spigot command handler method, this
     * will be executed by the server when a player
     * runs a command associated with the plugin.
     *
     * @param CommandSender sender
     * @param Command       command
     * @param String        label
     * @param StringArray   args
     * @return boolean
     */
    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args)
    {
        if (!(sender instanceof Player)) {
            plugin.getEnvoyer().sendMessage(sender, "Reloading configuration..");

            plugin.reloadConfig();
            plugin.loadMessages();

            plugin.getEnvoyer().sendMessage(sender, "The configuration has been reloaded successfully!");
            return true;
        }

        Player player = (Player) sender;

        if (!player.hasPermission(Permissions.RELOAD)) {
            plugin.getEnvoyer().missingPermission(player, Permissions.RELOAD);
            return false;
        }

        plugin.getEnvoyer().sendMessage(player, "&8[&aツ&8]&m-&8[ &aReloading CowJoinMessages..");

        plugin.reloadConfig();
        plugin.loadMessages();

        plugin.getEnvoyer().sendMessage(player, "&8[&aツ&8]&m-&8[ &aCowJoinMessages was reloaded successfully!");

        return true;
    }
}
