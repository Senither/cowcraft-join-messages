package com.sendev.cowjoinmessages;

import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;

public class ServerListener implements Listener
{

    /**
     * The main file instance, allows us to talk
     * to the server more easily, reload the
     * config and check messages.. etc..
     *
     * @var CowJoinmessages plugin
     */
    private final CowJoinMessages plugin;

    /**
     * Creates a new instance of the Server Listener
     * class, and sets our plugin object.
     *
     * @param CowJoinmessages plugin
     */
    public ServerListener(CowJoinMessages plugin)
    {
        this.plugin = plugin;
    }

    /**
     * This method will be executed by the Bukkit/Spigot
     * server when a player joins the server.
     *
     * @param PlayerJoinEvent e
     */
    @EventHandler
    public void onPlayerJoin(PlayerJoinEvent e)
    {
        // Turns off the default join message
        e.setJoinMessage(null);
        
        Player player = e.getPlayer();

        if (player.isOp()) {
            if (plugin.getDefaultOpJoinMessage() != null) {
                sendMessage(player, plugin.getDefaultOpJoinMessage());
            }
            return;
        }
        
        String message = plugin.getMessage(player);

        if (message != null) {
            sendMessage(player, message);
        }
    }

    /**
     * Sends the message off as a broadcast, in the
     * process all the placeholders will also be
     * replaced with real data.
     *
     * @param Player player
     * @param String message
     */
    private void sendMessage(Player player, String message)
    {
        plugin.getEnvoyer().broadcast(message
                .replace("{player}", player.getName())
        );
    }
}
