package com.sendev.cowjoinmessages;

import com.sendev.cowjoinmessages.utils.Envoyer;
import java.util.HashMap;
import java.util.Map;
import org.bukkit.entity.Player;
import org.bukkit.plugin.java.JavaPlugin;

public class CowJoinMessages extends JavaPlugin
{

    /**
     * A list of the permission nodes as keys, with
     * the colorized message associated with it.
     *
     * @var HashMap messages
     */
    private final HashMap<String, String> messages = new HashMap<>();

    /**
     * This message will act as the join messages for
     * players who are oped, this is "required" since
     * players who are op'ed will have every
     * permission by default.
     *
     * @var String defaultOpJoinMessage
     */
    private String defaultOpJoinMessage = null;

    /**
     * The global instance of our envoyer class, makes
     * it easier to send players and the console, messages.
     *
     * @var Envoyer envoyer
     */
    private Envoyer envoyer;

    /**
     * JavaPlugin onEnable method, this method
     * will run by itself when the plugin is loaded.
     *
     * @return void
     */
    @Override
    public void onEnable()
    {
        envoyer = new Envoyer(this);
        
        saveDefaultConfig();
        loadMessages();
        
        getServer().getPluginManager().registerEvents(new ServerListener(this), this);
        getCommand("cowjoinmessages").setExecutor(new CowReloadCommand(this));
    }

    /**
     * Will reset and re-load in the messages from
     * the configuration along with the permission
     * node it is linked to.
     *
     * @return void
     */
    public void loadMessages()
    {
        messages.clear();
        defaultOpJoinMessage = null;
        
        Map<String, Object> data = getConfig().getConfigurationSection("join-messages").getValues(false);
        
        for (String node : data.keySet()) {
            String permission = getConfig().getString("join-messages." + node + ".permission", null);
            String message = getConfig().getString("join-messages." + node + ".message", null);
            
            if (permission == null || message == null) {
                continue;
            }
            
            if (getConfig().getBoolean("join-messages." + node + ".opMessage", false)) {
                defaultOpJoinMessage = envoyer.colorize(message);
            }
            
            messages.put(permission, envoyer.colorize(message));
        }
    }

    /**
     * Gives you all the messages and their permission node.
     *
     * @return HashMap
     */
    public HashMap<String, String> getMessages()
    {
        return messages;
    }

    /**
     * Gives you the join message the player has access
     * to, if they don't have permission for any of the
     * join messages, the method will return null.
     *
     * @param Player player
     * @return String|null
     */
    public String getMessage(Player player)
    {
        for (String node : getMessages().keySet()) {
            if (player.hasPermission(node)) {
                return getMessages().get(node);
            }
        }
        return null;
    }

    /**
     * Gets the global envoyer class, allowing us to
     * talk to players more easily.
     *
     * @return Envoyer
     */
    public Envoyer getEnvoyer()
    {
        return envoyer;
    }

    /**
     * Gets the default operator join message.
     *
     * @return String
     */
    public String getDefaultOpJoinMessage()
    {
        return defaultOpJoinMessage;
    }
    
}
